package com.donsaldosan.cursospringboot.controllers;

import com.donsaldosan.cursospringboot.dao.UsuarioDao;
import com.donsaldosan.cursospringboot.models.Usuario;
import com.donsaldosan.cursospringboot.utils.JWTUtil;
import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
public class UsuarioController {

    @Autowired
    private UsuarioDao usuarioDao;

    @Autowired
    private JWTUtil jwtUtil;

    @RequestMapping(value = "api/usuario/{id}")
    public Usuario getUsuario(@PathVariable Long id){
        Usuario usuario = new Usuario();
        usuario.setId(id);
        usuario.setNombre("Ramiro");
        usuario.setApellido("Cazales");
        usuario.setEmail("ramicaz@gamil.com");
        usuario.setTelefono("123456789");
        return usuario;
    }
    @RequestMapping(value = "api/usuarios")
    public List <Usuario> getUsuarios(@RequestHeader(value="Authorization") String token){
        String usuarioId = jwtUtil.getKey(token);
        if(!validarToken(token)){            return null;        }
        return usuarioDao.getUsuarios();
    }
    public boolean validarToken(String token){
        String usuarioId = jwtUtil.getKey(token);
        return usuarioid != null;
    }
    @RequestMapping(value = "api/usuarios", method = RequestMethod.POST)
    public void registrarUsuario(@RequestBody Usuario usuario){
        Argon2 argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2id);
        String hash = argon2.hash(1, 1024, 1, usuario.getPassword());
        usuario.setPassword(hash);
        usuarioDao.registrar(usuario);
    }
    @RequestMapping(value = "editar/{id}")
    public Usuario editar(@PathVariable Long id){
        Usuario usuario = new Usuario();
        usuario.setId(id);
        usuario.setNombre("Ramiro");
        usuario.setApellido("Cazales");
        usuario.setEmail("ramicaz@gamil.com");
        usuario.setTelefono("1234567890");
        return usuario;
    }
    @RequestMapping(value = "api/usuarios/{id}", method = RequestMethod.DELETE)
    public void eliminar(@RequestHeader(value="Authorization") String token, @PathVariable Long id){
        if(!validarToken(token)){ return; }
        usuarioDao.eliminar(id);
    }

}
