// Call the dataTables jQuery plugin
$(document).ready(function() {
    cargarUsuarios();
  $('#usuarios').DataTable();
});

async function cargarUsuarios() {
    const request = await fetch('api/usuarios', {
        method: 'GET',
        headers: getHeaders()
    });
    const usuarios = await request.json();
    let listadohtml = '';
    console.log(usuarios);
    for (usuario of usuarios){
        let telefonoTexto = usuario.telefono== null ? '-': usuario.telefono;
        let botonEliminar = '<a href="#" onclick="elimnarUsuario('+usuario.id+')" class="btn btn-danger btclassNameNamecle btn-sm"><i class="fas fa-trash"></i classNameName </a>';
        let usuariohtml ='<tr><td>'+usuario.id+'</td><td>'+usuario.nombre+' '+ usuario.apellido+' </td><td>'+usuario.email+'</td> <td>'+
            telefonoTexto+'</td><td> '+ botonEliminar +'</td></tr>';
        listadohtml += usuariohtml;
    }
    document.querySelector('#usuarios tbody').outerHTML = listadohtml;


}
function getHeaders(){
    return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': localStorage.token
    }
}

async function elimnarUsuario(id){
    if (!confirm('¿Desea eliminar este usuario?')){
        return;
    }

    const request = await fetch('api/usuarios/'+id, {
        method: 'DELETE',
        headers: getHeaders()
    });
    location.reload()
}